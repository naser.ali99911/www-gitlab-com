---
layout: markdown_page
title: "Category Direction - Certify"
description: View the group strategy for the Certify group, part of the plan stage.
canonical_path: "/direction/plan/certify/"
---

- TOC
{:toc}

## Certify Group

**Last Updated:** 2023-02-06

### Introduction

Certify Group focuses on Requirements and Traceability Management. Requirements and Traceability Management can be broken into two components, requirements and quality. At GitLab we believe we can create a positive experience for our customers by creating a solution allowing users to trace their requirements workflow in a single platform.

Requirements Management is a process by which these behaviors would be captured so that there is a clearly defined scope of work. A good general overview is provided in an [article from PMI](https://www.pmi.org/learning/library/requirements-management-planning-for-success-9669). For less restrictive environments, Requirements Management can take the form of jobs to be done (JTBD) statements, which are satisfied through iterative improvements or additional features.

The Requirements and Quality Management software fields are full of well-rounded and mature vendors, including long-standing solutions like IBM DOORS, and newer solutions pushing the boundaries of the fields like Jama, Modern Requirements, and Xray. As a buyer, it can be challenging to navigate Why one vendor is a better solution than another. Here are the reasons Why we believe GitLab can be the standout tool, and your tool of choice, in these areas:

1. **Simplified Development Toolchain:** Instead of pulling together point solutions to manage requirements and quality, and constantly juggling all those tools and homegrown scripts, your team can focus on developing and delivering the code that matters with requirements and quality management capabilities built right into GitLab. For example, a company currently leverages IBM DOORS, Xray, Jira, and GitHub for its development. With GitLab, they can reduce their tool sprawl and DevSecOps "tax" via our single DevSecOps platform. With an all-in-one solution, companies can increase their efficiency, simplify their development, and ultimately win more with their customers.
2. **Improved Developer Workflow via Native Traceability:** Traceability is the capacity for users to see the most up-to-date status and data related to a requirement, both upstream and downstream. With GitLab, we believe we can provide a unique and comprehensive level of traceability given our platform approach. With users no longer jumping from solution to solution to understand the status of a test, how an Issue is linked to a requirement, or who authored a recent requirement, they can more efficiently view and understand their most up-to-date information. An example of this may be a company using Xray and Jira, who may be managing their User Stories in Jira but then have to jump back into Xray to view reports and dashboards on their testing. With GitLab, this information lives within a single platform, simplifying the development process and visibility into existing statuses. Simplified, our vision is to create a singular source-of-truth platform to manage, understand, and improve your requirements and quality management process. 
3. **Cost Savings:**  Reducing your application toolchain not only minimizes complexity, but can also save your organization money. Customers utilizing GitLab Ultimate, which is where  Requirements capabilities are available in, can already use requirements and quality capabilities. Those using GitLab Premium or our Free edition may be surprised to learn of the advantages and additional requirements and quality management capabilities that are gained by moving to our Ultimate option. 

### Strategy and themes

Our current Strategy (FY 2024 Q1) is centered around creating a requirements Work Item type within the GitLab planning framework by [migrating legacy GitLab requirements functionality](https://gitlab.com/gitlab-org/plan/-/issues/735) from an independent object to a general Work Item type. This strategy is part of a broader initiative in the Plan Stage to create greater continuity between Work Item types (such as Requirements, Issues, Epics, and Tasks). Work Items will ultimately allow customers to integrate requirements directly into their planning process and include functionality such as labels, comments, tagging, and more, subsequently improving traceability.

Our primary strategy centers around the theme of traceability. An example given is tracing requirements to test cases in both a manual and automated fashion, creating more robust test case and test session capabilities, and allowing more traceability in the way in which requirements are audited. 

### 1 year plan

Our 1 year plan is for Certify to be a viable option for the [Software Developer](/handbook/product/personas/#sasha-software-developer) and [Software Engineering in Test](/handbook/product/personas/#simone-software-engineer-in-test) personas. We plan to arrive as that viable option in several ways:

- GitLab continues to support larger enterprises, and the natural need for multiple levels of requirements and test cases which can be decomposed down to requirements or test cases at lower levels has risen. Our objective is to expand our Work Item definition to allow for multi-level objects. This would allow teams to create a system of sub-systems and perform all requirement tracing and test tracing directly within GitLab, further adhering to our mission as The One DevSecOps Platform.
- We recognize that requirements and their associated trace data is often required as release evidence / artifacts. We would like to work closely with our release team to integrate requirements traceability into release evidence.
- Visual representation of traceability and test coverage is also of importance. We would like to provide a visual representation of ancestors and descendants of requirements, making it easy to visualize decomposition and traceability. It would also be ideal for passing / failing test results to roll up visually to the requirements, allowing for quick visualization of the requirement status with regards to implementation and verification.
At present, both [Quality Management](#quality-management) and [Requirements Management](#requirements-management) categories are considered at [minimal maturity](/direction/maturity/).

### What we recently completed

We've previously completed work allowing users to create and manage both requirements and test cases within GitLab. It is our fundamental belief that for maximum efficiency and reduced cycle time users should be able to complete their work in a single cohesive application. This platform arpproach reduces the overall mental strain of switching between applications, while also allowing opportunity for automation.

In the last 3 months we've focused specifically on migration legacy requirements functionality to a Work Item type. We've been tracking this work via the following [Epic](https://gitlab.com/groups/gitlab-org/-/epics/5171). Some specific highlights include allowing users the ability to [reference works items of requirements type from test reports](https://gitlab.com/gitlab-org/gitlab/-/issues/329435) as well as (filtering requirement work items by status on GraphQL](https://gitlab.com/gitlab-org/gitlab/-/issues/366218). For more details please reference our planning breakdown Issues from [%15.8](https://gitlab.com/gitlab-org/plan/-/issues/767), [%15.7](https://gitlab.com/gitlab-org/plan/-/issues/735), and [%15.6](https://gitlab.com/gitlab-org/plan/-/issues/710).

### What we are currently working on

Certify Group continues to focus on the migration of Requirements to a Work Item type in milestone 15.9. Movement of Requirements to a Work Item type provides more flexibility in the project management workflow, allowing users to better trace and manage their work between different object types.

Work for milestone 15.9 can be broken into two categories:

First, Deprecating and Removing the Dedicated Requirement Object for Work Items Migration. Focusing on removing the pre-existing Requirements object as part of the transition to a Work Item type. The following issues address this first area.

[GraphQL: Expose legacy requirement IID under Work Item Requirement type in GraphQL](https://gitlab.com/gitlab-org/gitlab/-/issues/383016)
[GraphQL: Enable legacy IID filtering under Work Items GraphQL query](https://gitlab.com/gitlab-org/gitlab/-/issues/383017)
[Create "test reports" widget for requirements work item type](https://gitlab.com/gitlab-org/gitlab/-/issues/382590)
[Use Work Items GraphQL query in Requirements UI index page](https://gitlab.com/gitlab-org/gitlab/-/issues/382265)
[Use Work Items GraphQL query for requirements count in UI](https://gitlab.com/gitlab-org/gitlab/-/issues/382469)

Second, Requirements CSV Import/Export for Work Item Parity with the current Requirements Management experience. The following issues address this second area.

[Work Items: Restructure CSV export/import classes](https://gitlab.com/gitlab-org/gitlab/-/issues/379081)
[Work Items: Export basic data](https://gitlab.com/gitlab-org/gitlab/-/issues/379082)
[Work Items: Import basic data](https://gitlab.com/gitlab-org/gitlab/-/issues/379153)

### What is next for us

Our upcoming work continues to focus on the migration of requirements to a Work Item type which will provide a more integrated requirements management experience for GitLab users. For additional context, including a breakdown of the work we are completing around requirements import and export capablities, requirements hierarchies, and requirements permissions please view the following [Epic](https://gitlab.com/groups/gitlab-org/-/epics/5171).

### What we are not doing

Our current workload is focused 85% on the transition of requirements to a Work Item type, leaving minimal capacity for maturation of quality management components such as test cases. Our plan is to complete the requirements transition to a Work Item type by the end of FY24 Q1 and then begin planning a more mature user experience with test cases, sessions, and reporting woven into the requirements experience.

### Competitive Analysis and Best-in-class Landscape

#### Requirements Management Competitive Overview
 
Requirements Management Category Best In Class Competitor

1. Based on our analysis, customer conversations, analyst reports, and feature-to-feature comparison, we've identified [Jama](https://www.jamasoftware.com/) as the Best In Class (BIC) competitor over IBM DOORS and Modern Requirements. Jama was identified as BIC for several reasons, including its more robust integration ecosystem with popular project management tools, such as Jira, allowing it broader capacity to serve users and their needs. Additionally, Jama was [proven](https://resources.jamasoftware.com/infographic/g2-recognizes-jama-connect-as-the-only-leader-in-requirements-management?_gl=1*1mtt0tr*_ga*MzU4MjkzNDQxLjE2NjY4MDU3MjI.*_ga_JQG3M83PDQ*MTY3MzQ1NzkwMy4yMC4wLjE2NzM0NTc5MDMuNjAuMC4w), vs IBM DOORS, to help organizations achieve ROI 45% faster and onboard users 2.7 times faster, and was rated as easier to use from an admin and end-user perspective. We are leveraging the following [Epic](https://gitlab.com/groups/gitlab-org/-/epics/4669) to fill the critical gaps between GitLab and Jama today. 

Top 3 Competitors

1. [Jama](https://www.jamasoftware.com/) is a live requirement traceability solution founded in 2007 and based out of Portland, OR. Jama has more than 12.5 million customers in a wide variety of industries including aerospace and defense, automotive, software development, financial services and insurance, government, industrial manufacturing, medical device and life sciences, and semiconductors. Jama Connect solutions serve several roles, including requirements management, requirements traceability, risk management, test management, and MBSE. Jama has frequently been named a leader in Requirements Management Software. See our [roadmap](https://gitlab.com/groups/gitlab-org/-/epics/4669) for GitLab to compete with [Jama](https://www.jamasoftware.com/).
2. [IBM DOORS](https://www.ibm.com/products/requirements-management-doors-next?r=rmt&dpm=39463)  is a requirements management tool founded in 1991 with its current headquarters in New York, U.S.A. IBM DOORS (an acronym for Dynamic Object-Oriented Requirements Systems) is commonly referenced as a leading requirements management tool enabling users to capture, manage, trace, and analyze requirements. IBM DOORS allows the capacity to link requirements to test cases and plans, design items, and other requirements. IBM DOORS, and its web-based version IBM DOORS Next Generation, serve a variety of industries, including aerospace and defense, government, and medical device and life sciences.
3. [Modern Requirements](https://www.modernrequirements.com/), founded in 2006 and based out of Ontario, Canada is a requirements management tool built for Azure DevOps. Modern Requirements formed a partnership with Microsoft in 2010, allowing users to manage, create, and report on requirements directly from an Azure DevOps project. Its customers include healthcare and medical devices, banking and insurance, government and defense, automotive, aerospace, and services and technology industries.

Key Features for Comparison

1. **Live Traceability:** Live traceability is the capacity for users, at any time, to see the most up-to-date status and data related to a requirement, both upstream and downstream. Live traceability is paramount to ensuring requirements are hitting industry standards and product safety requirements. This aligns with the JTBD, "when collaborating with a team or stakeholders, I want to radiate the current status of work continuously, so I can increase alignment on progress and any impediments that need to be addressed."
2. **Requirement Workflow Management:** Requirement workflow management can be defined as the capacity to create, review, approve, reject, satisfy, complete, delete, or adjust a requirement. In a mature requirement workflow management tool, users should be able to move requirements from one state to another within the workflow.
3. **Real-Time Collaboration on Requirements:** Real-time collaboration on requirements is the capacity to communicate on, strategize, and relay the most up-to-date information on a requirement within the given solution. This aligns with the JTBD, "when soliciting feedback and collaboration on the implementation of my strategy, I want to craft a view of my plan with the intended audience in mind so that I can increase team and stakeholder alignment through effective storytelling at the correct level of abstraction."
4. **Multi-Level Hierarchies for Requirements:** Multi-level hierarchies for requirements is the capacity to create stacked requirements that allow users to break down larger initiatives and features into smaller blocks. This is similar to how GitLab manages hierarchies for Epics. This aligns with the JTBD, "when splitting prioritized initiatives or features into requirements, I want to group related slices of value and surface dependencies, so I can maximize alignment on the scope of a business goal and efficiently plan its incremental delivery."
5. **Native Requirement Authoring Assistance:** Requirement authoring assistance is automated active feedback on written requirements as checked against authoring languages. An effective requirement authoring assistant can provide feedback on areas to improve your requirement language to better meet pre-defined authoring structures.
6. **Advanced Requirement Reporting:** Requirements reporting provides context on the current status of requirements, the relationship between requirements and other objects, users associated with requirements, and a host of other data points related. This aligns with the JTBDs of, "when reviewing my product strategy with leadership, I want to demonstrate how my plans will drive overall vision and objectives toward reality, so I can increase buy-in, trust, and allocation of resources toward my efforts" and "when analyzing past releases, I want to analyze how successful deliverables were in satisfying objectives, so I can decrease the time it takes to course correct and improve future plans."
7. **Customizable Requirement Statuses:** Customizable requirement Statuses allows users to adjust their requirement status options so they are relevant to their workflow. Some users may have a preference for simplified status options (unsatisfied or satisfied), while others may want more options (unsatisfied, satisfied, inactive, not checked again, etc). This aligns with the JTBD, "when monitoring progress, I want to ensure the current scope and status of work are thoroughly captured, so I can increase the accuracy of my reporting and maintain trust with stakeholders."
8. **Built-in Test Management Center:** More mature requirements tools combine test case and test session capabilities that fall under the quality management category into their solution. Their test management center provides visibility into the active Statuses of tests and a host of other data points.
9. **Multi-format Import and Export Capabilities:** This feature allows users to import and export their requirements in a variety of formats. This feature is valuable in many situations, including audits where documentation may need to be provided outside the solution for verification purposes.
10. **Templated and/or Reusable Requirements:** This feature is similar to templated Issues 🟩 in GitLab today but with a leantoward requirements. Users may frequently create and re-use a requirement (for example, in the building of a plane type multiple times). Templates allow users to be more precise with their process and align with the JTBD, "when fulfilling requirements, I want to build quality and maintain quality, so I can increase confidence that I am delivering value to stakeholders."

GitLab vs Jama/IBM DOORS/and Modern Requirements

1. GitLab versus [Jama](https://www.jamasoftware.com/): 

| Feature | GitLab | Jama |
| ------ | ------ | ------ |
|     Live Traceability   | 🟨  |  🟩 |
|    Requirements Workflow Management    | 🟩 | 🟩 |
|    Real-Time Collaboration on Requirements    | 🟩 | 🟩 |
|   Multi-Level Hierarchies for Requirements     | ⬜️  | 🟩 |
|   Native Requirements Authoring Assistance    | ⬜️  | 🟩 |
|    Requirement Reporting    | ⬜️ | 🟩 |
|   Customizable Requirement Status'     | 🟨 | 🟩 |
|    Built-in Test Management Center    | 🟨 | 🟩 |
|    Multi-format Import and Export Capabilities    | ⬜️ | 🟩 |
|    Templated and/or Reusable Requirements    | ⬜️ | 🟩 |
|   Full DevSecOps Platform Solution  | 🟩 |  ⬜️ |

2. GitLab versus [IBM DOORS](https://www.ibm.com/products/requirements-management-doors-next?r=rmt&dpm=39463):

| Feature | GitLab | IBM DOORS |
| ------ | ------ | ------ |
|     Live Traceability   | 🟨 |  🟩  |
|    Requirements Workflow Management    | 🟩 | 🟩 |
|    Real-Time Collaboration on Requirements    | 🟩 |  🟩  |
|   Multi-Level Hierarchies for Requirements     | ⬜️ | 🟩 |
|    Native Requirements Authoring Assistance    | ⬜️ | ⬜️ |
|    Requirement Reporting    | ⬜️  | 🟩 |
|   Customizable Requirement Status     | 🟨   |Unknown|
|    Built-in Test Management Center    | 🟨 | 🟩 |
|    Multi-format Import and Export Capabilities    | ⬜️ | 🟩 |
|    Templated and/or Reusable Requirements    | ⬜️ | 🟩 |
|   Full DevSecOps Platform Solution  | 🟩 | ⬜️ |

3. GitLab versus [Modern Requirements (Azure DevOps)](https://www.modernrequirements.com/):

| Feature | GitLab | Modern Requirements |
| ------ | ------ | ------ |
|     Live Traceability   | 🟨 | 🟩 |
|    Requirements Workflow Management    | 🟩 | 🟩 |
|    Real-Time Collaboration on Requirements    | 🟩 | 🟩 |
|   Multi-Level Hierarchies for Requirements     | ⬜️  | 🟩 |
|   Native Requirements Authoring Assistance    | ⬜️ | 🟩 |
|    Requirement Reporting    | ⬜️  | 🟩 |
|   Customizable Requirement Status'     |🟨 | 🟩 |
|    Built-in Test Management Center    | 🟨 |Unknown |
|    Multi-format Import and Export Capabilities    | ⬜️ | 🟩 |
|    Templated and/or Reusable Requirements    | ⬜️ | 🟩 |
|   Full DevSecOps Platform Solution  | 🟩  | ⬜️ |

#### Quality Management Competitive Overview

Quality Management Category Best In Class Competitor

1. Based on our analysis, analyst reports, and customer conversations, we've identified [Xray](https://www.getxray.app/test-management?utm_term=xray%20jira%20plugin&utm_campaign=Search+-+Xray+US+-+Branded+-+Xray&utm_source=adwords&utm_medium=ppc&hsa_acc=9970092548&hsa_cam=9413622843&hsa_grp=128282729421&hsa_ad=570822222864&hsa_src=g&hsa_tgt=kwd-923184782077&hsa_kw=xray%20jira%20plugin&hsa_mt=e&hsa_net=adwords&hsa_ver=3&gclid=Cj0KCQiAtvSdBhD0ARIsAPf8oNkV8WBUHz0uSTFWjE781c0WbQTW61RxKhR75QxvYu2ONAd61rIAk_caAqyjEALw_wcB) as the Best In Class Competitor. We are leveraging the following [Epic](https://gitlab.com/groups/gitlab-org/-/epics/4670) to fill critical gaps between GitLab and Xray. We've also identified two other competitors and key features to compare against (illustrated further in this section.)

Top 3 competitors

1. [Xray](https://www.getxray.app/) is a native test management solution founded in 2013 and based out of Amadora, Portugal. Xray currently has more than 5,000 companies using its solution and over 5.6 million users. Xray provides seamless test management by integrating directly with popular solutions, including GitLab and Jira. Xray is capable of linking requirements directly to test cases, producing traceability reports, and has a Jira-native experience for Jira users. [See the deep dive comparison](https://docs.google.com/document/d/144MucUAUiy5fFC0ClHzJq2vohZT9-pYlYwKNOB-dlV4/edit#heading=h.dfm02ebhejqz).
2. [TestRail](https://www.gurock.com/testrail) is a solution provided by Gurock Software, founded in 2004 with offices across the globe. TestRail has more than 100,000 users and has the same parent organization as Xray (Idera). TestRail is pegged as a leader in test management software with a designated market segment mainly consisting of mid-market and small-business customers. With TestRail you can create, organize, and manage tests throughout your entire testing process. In addition, real-time insights within TestRail provide an opportunity for improvements in productivity and efficiency.
3. [PractiTest](https://www.practitest.com/) is an end-to-end test management platform founded in 2008. Similar to Xray and TestRail it integrates with project management solutions tools such as Jira, GitHub, and GitLab. PractiTest provides a centralized location for test management, including requirements, tests, issues, TestSets, and runs. Its customer base spans a myriad of industries.

Key Features to Compare Against

1. **Requirements and User Stories Management:** Requirements and users story management is the capacity to complete processes more granularly defined in requirements management tools such as Jama or IBM DOORS. An example of an ability in this feature is the support for hierarchical requirements.
2. **Test Case Management:** Test case management is the process of creating, planning, running, monitoring, and reporting upon tests. Some more granular examples of Test Case Management include adding attachments to test steps, cloning tests between projects, reusing tests, and exploratory testing sessions.
3. **Test Sets, Sessions, Suites, and Execution Management:** Provides audit history of runs/results, the ability to edit tests during runs, context around why a test failed or passed and parameterization of test cases and sets.
5. **Test Dashboard and Reporting:** Feature providing feedback on tests as well as the capacity to schedule reports.
6. **Ability to Import and Export Test Data:** Similar to Requirements it's important for users to be able to import and export test data for a myriad of reasons, including auditing purposes.
7. **Test Search Capability within the Solution:** Users often generate large sums of tests over time and can find themselves digging through Tests to find problems or solutions. The capability to search across tests saves users time spent manually scrolling and reading through tests.
8. **Cloud or On-Prem**
9. **Test Library:** Test libraries provide an area to create and manage test cases and sessions. Test libraries provide live data on test runs and real-time feedback that is critical to ensuring systems and requirements are properly checked.

GitLab vs Xray/TestRail/ and PractiTest
 
1. GitLab versus [Xray](https://www.getxray.app/): 

| Feature | GitLab | Xray |
| ------ | ------ | ------ |
|    Requirement and User Stories Management   |    🟩    | 🟨      |
|    Test Case Management    |    🟨     |   🟩      |
|    Test Sets, Sessions, Suites, and Execution Management    |   🟨      |  🟩      |
|   Customizable Workflow, Fields, and Filters    |    🟨    |   🟩      |
|   Test Dashboard and Reporting   |     ⬜️     |      🟩     |
|    Ability to Import and Export Test Data    |     ⬜️      |      🟩     |
|  Test Search Capacity within Solution    |  🟨     |    🟩      |
|    Cloud and On-Prem Solution  🟩     |      🟩     |      🟩     |
|    Test Library  |   🟨       |   🟩        |
|    Full DevSecOps Platform Solution  |   🟩       |     ⬜️      |


2. GitLab versus [TestRail](https://www.gurock.com/testrail): 

| Feature | GitLab | TestRail |
| ------ | ------ | ------ |
|    Requirement and User Stories Management   |     🟩      |   🟨        |
|    Test Case Management    |     🟨      |   🟩        |
|    Test Sets, Sessions, Suites, and Execution Management    |   🟨       |    🟩      |
|   Customizable Workflow, Fields, and Filters    |     🟨      |    🟩      |
|   Test Dashboard and Reporting   |     ⬜️     |       🟩   |
|    Ability to Import and Export Test Data    |     ⬜️      |   🟨        |
|  Test Search Capacity within Solution    |  🟨         |     🟩     |
|    Cloud and On-Prem Solution  🟩     |      🟩    |   🟩        |
|    Test Library  |   🟨        |     🟩     |
|    Full DevSecOps Platform Solution  |    🟩     |     ⬜️     |

3. GitLab versus [PractiTest](https://www.practitest.com/): 

| Feature | GitLab | PractiTest |
| ------ | ------ | ------ |
|    Requirement and User Stories Management   |     🟩      |   🟩      |
|    Test Case Management    |    🟨    |     🟩    |
|    Test Sets, Sessions, Suites, and Execution Management    |   🟨       |     🟩     |
|   Customizable Workflow, Fields, and Filters    |     🟨      |   🟩      |
|   Test Dashboard and Reporting   |     ⬜️      |   🟩       |
|    Ability to Import and Export Test Data    |     ⬜️       |    🟨       |
|  Test Search Capacity within Solution    |   🟨         |   🟩       |
|    Cloud and On-Prem Solution  🟩     |      🟩    |   ⬜️      |
|    Test Library  |   🟨       |    🟩       |
|    Full DevSecOps Platform Solution  |    🟩       |     ⬜️     |

### Maturity Plan

| Category                | Maturity Plan | Description | Current Maturity as of 2022-02-06 |
| :---                    | :---      | :---        | :---:    |
| Requirements Management | [Requirements Management Maturity Plan](https://gitlab.com/groups/gitlab-org/-/epics/4668) | Manage functional requirements within GitLab | [minimal](/direction/maturity/) |
| Quality Management | [Quality Management Maturity Plan](https://gitlab.com/groups/gitlab-org/-/epics/4668) | Manage and trace test cases within GitLab | [minimal](/direction/maturity/) |

### Target audience

Certify's target audience focuses on three key personas as aligned against GitLab's [personas framework](https://about.gitlab.com/handbook/product/personas/).

Requirements Owners: Those who create, approve, and manage requirements. They may also utilize requirements information to report up and out to stakeholders. Example roles may include Product Managers, Project Managers, Development Team Leads, and Release Managers.

Makers: Those who write the code that looks to satisfy the requirements. Example roles may include Software Developers and Product Designers.

Testers: Those who manage, create, archive, and articulate tests as set against requirements. Example roles may include Software Engineers in Test and QA Engineers.
